// Raqueta.cpp: implementation of the Raqueta class.
//AUTOR: RICARDO ANDRES BAÑOS ROJAS
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	velocidad.x = 0;
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	x1 += 0;
	x2 += 0;
	y1 += velocidad.y * t;
	y2 += velocidad.y * t;
}

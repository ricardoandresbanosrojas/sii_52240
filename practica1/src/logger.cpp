//logger.cpp
//AUTOR: RICARDO ANDRÉS BAÑOS ROJAS
//////////////////////////////////////////////////////////////////////

#include <stdio.h> 
#include <stdlib.h>
#include <string.h> 
#include <fcntl.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <unistd.h>

#include <iostream>

#define B_SIZE 2 //1er byte = identificador del jugador; 2º byte = num puntos totales del jugador (hasta 255)

void CrearFIFO();
void LecturaFIFO(int fileDescriptor);

int main (int argc, char *argv[])
{	
	int fd;

	CrearFIFO();

	fd = open("logger.txt", O_RDONLY);
	
	LecturaFIFO(fd);

	int verificacion;
	verificacion = unlink("logger.txt");
}

void CrearFIFO()
{
	mkfifo("logger.txt", 0666);
}

void LecturaFIFO(int fileDescriptor)
{
	char buf[2];
	bool sigueOperando = true;
	
	while (sigueOperando)
	{
		if (read(fileDescriptor, buf, B_SIZE) > 0)
			std::cout << "Jugador " << buf[0] << " marca 1 punto, lleva un total de " << buf[1] << " puntos." << std::endl;	

		else if (read(fileDescriptor, buf, B_SIZE) == 0)
			sigueOperando = false;
	}
}

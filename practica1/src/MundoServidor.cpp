//Mundo.cpp: implementation of the CMundo class.
//AUTOR: RICARDO ANDRÉS BAÑOS ROJAS
//////////////////////////////////////////////////////////////////////
#include <fcntl.h> 
#include <sys/stat.h>
#include <sys/mman.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <errno.h>
#include <pthread.h>

#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d);

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close(fdFifoLogger);
	close(fdFifoCoordenadas);
	close(fdFifoTeclas);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.NuevasDimensiones(0.025f);

	int i;

	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		//ESCRIBIR EN FIFO QUE JUGADOR HA MARCADO Y CUANTOS PUNTOS TOTALES TIENE
		bufFifoLogger[0] = '2';

		char c = static_cast<char>(puntos2);
		bufFifoLogger[1] = c;

		write(fdFifoLogger, bufFifoLogger, sizeof(char)*2);

		if(puntos2 >= 3)
		{	
			std::cout << "Jugador 2 gana la partida" << std::endl;
			JUEGO_TERMINA = 1;
		}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		//ESCRIBIR EN FIFO QUE JUGADOR HA MARCADO Y CUANTOS PUNTOS TOTALES TIENE
		bufFifoLogger[0] = '1';

		char c = static_cast<char>(puntos1);
		bufFifoLogger[1] = c;

		write(fdFifoLogger, bufFifoLogger, sizeof(char)*2);
		
		
		if(puntos1 >= 3)
		{	
			std::cout << "Jugador 1 gana la partida" << std::endl;
			JUEGO_TERMINA = 1;
		}

	}

	//FIFO COORDENADAS
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2);

	write(fdFifoCoordenadas, cad, sizeof(char)*200);
}

void CMundo::Init()
{
	JUEGO_TERMINA = 0;

	//FIFO LOGGER
	fdFifoLogger = open("logger.txt", O_WRONLY);

	if(fdFifoLogger == -1)
		perror("Open fdFifoLogger fail");
	
	//FIFO COORDENADAS
	fdFifoCoordenadas = open("coordenadas.txt", O_WRONLY);

	if(fdFifoCoordenadas == -1)
		perror("Open fdFifoLogger fail");

	//FIFO TECLAS
	fdFifoTeclas = open("teclas.txt", O_RDONLY);
	
	if(fdFifoTeclas == -1)
		perror("Open fdFifoTeclas fail");	
	
	//THREAD
	int ver;	
	
	ver = pthread_create(&thread1, NULL, hilo_comandos, this);
	
	if(ver)	
		perror("Pthread create fail");
	
	//////////////////////////////////////////////////////////
	Plano p;

	//Pared Inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//Pared Superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//A la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//A la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

void CMundo::RecibeComandosJugador()
{
	while (1) 
	{
        	usleep(10);
        	char tecla;

        	read(fdFifoTeclas, &tecla, 1);

        	unsigned char key;
		
		key = tecla;

        	if(key == 's')jugador1.velocidad.y=-4;
        	if(key == 'w')jugador1.velocidad.y=4;
        	if(key == 'l')jugador2.velocidad.y=-4;
        	if(key == 'o')jugador2.velocidad.y=4;
	}
}

void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}

